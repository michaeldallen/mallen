.ONESHELL:

.PRECIOUS: cache/%.stl cache/%.png

SHELL := bash

INDENT := 2>&1 | sed 's/^/    /'

make.targets :
	@echo "available Make targets:"
	@$(MAKE) -pRrq -f $(firstword $(MAKEFILE_LIST)) : 2>/dev/null \
	| awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}} /^# Implicit Rules/,/^# [0-9]+ implicit rule/ {if ($$1 !~ "^[#.]") {print $$1}}' \
	| egrep -v '^make.targets$$' \
	| sed 's/^/    make    /' \
	| env LC_COLLATE=C sort


clean :
	@echo liness is next to Godliness


all :
	@for d in Dockerfile.* ; do echo ${MAKE} $$d ; done

IAM := registry.gitlab.com/michaeldallen/mallen


docker.%.latest :
	@target=$$(echo $@ | sed 's/^docker.\(.*\).latest/\1/')
	@echo docker build -f Dockerfile.$${target} -t ${IAM}/$${target}:latest .
	@echo docker push ${IAM}/$${target}:latest

docker.%.dated :
	@target=$$(echo $@ | sed 's/^docker.\(.*\).dated/\1/')
	@echo docker build -f Dockerfile.$${target} -t ${IAM}/$${target}:$$(date +%Y%m%d) .
	@echo docker push ${IAM}/$${target}:$$(date +%Y%m%d)

#EOF

